<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cheick;
use Auth;
use Str;
class CheickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cheicks = Cheick::all();
        return view('backoffice.cheicks.index',compact('cheicks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.cheicks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $cheick = new Cheick();
        $cheick->nom = $request->nom;
        $cheick->prenom = $request->prenom;
        $cheick->telephone = $request->telephone;
        $cheick->fonction = $request->fonction;
        $cheick->kouniah = $request->kouniah;
        $cheick->email = $request->email;
        $cheick->parcours = $request->parcours;
        $cheick->save();
        return redirect(route('cheick.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cheick = Cheick::find($id);
        return view('backoffice.cheicks.edit',compact('cheick'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
