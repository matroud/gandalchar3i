<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dourous;
use App\Models\Categorie;
use App\Models\Dars;

class PagesController extends Controller
{
    //

    public function index()
    {
        return view('index');
    }

    public function dourous()
    {
        return view('pages.dourous');
    }

    public function dars($slug)
    {
        $dars =  Dourous::where('slug', $slug)->first();
        $cours =  Dars::where('dourous_id', $dars->id)->get();
        $categories = Categorie::all();
        views($dars)->record(); // enregistrer le nombre de vues sur une page
        return view('pages.dars',compact('dars','categories','cours'));
    }

    public function categorie_dourous($slug)
    {
        $categorie = Categorie::where('slug_categorie',$slug)->first();
        $dourous = Dourous::where('categorie_id',$categorie->id)->get();
        return view('pages.dourous_categorie',compact('dourous'));
    }
    public function conferences()
    {
        return view('pages.conferences');
    }

    public function coran()
    {
        return view('pages.coran');
    }
}
