<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dourous;
use App\Models\Categorie;
use App\Models\Langue;
use App\Models\Niveau;
use App\Models\Chaine;
use App\Models\Cheick;
use App\Models\Dars;
use Auth;
use Str;

class DourousController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dourous = Dourous::all();
        return view('backoffice.dourous.index',compact('dourous'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categorie::all();
        $langues = Langue::all();
        $niveaux = Niveau::all();
        $chaines = Chaine::all();
        $cheicks = Cheick::all();
        return view('backoffice.dourous.create',compact('categories','niveaux','langues','chaines','cheicks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $dourous = new Dourous();
  
        if ($request->file('document')) {
            $document = $request->file('document');
            $link_document = Str::slug($request->title).'.'.$document->extension();
            $dourous->document =  '/uploads/dourous/document/'.$link_document;
            $document->move('uploads/dourous/document', $link_document);
        } 
        if ($request->file('image')) {
            $image_dars = $request->file('image');
            $link = Str::slug($request->title).'.'.$image_dars->extension();
            $dourous->image =  '/uploads/dourous/'.$link;
            $image_dars->move('uploads/dourous/', $link);

        }
        $dourous->title = $request->title;
        $dourous->prerequi = $request->prerequis;
        $dourous->slug = Str::slug($request->title,'-');
        $dourous->categorie_id = $request->categorie_id;
        $dourous->niveau_id = $request->niveau_id;
        $dourous->langue_id = $request->langue_id;
        $dourous->chaine_id = $request->chaine_id;
        $dourous->cheick_id  = $request->cheick_id;
        $dourous->format = 'audio';
        $dourous->etat_cours = $request->etat_cours;
        $dourous->created_by = Auth::user()->id;
        $dourous->save();
        return redirect(route('dourous.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($dourou)
    {
        $dourous = Dourous::where('slug',$dourou)->first();
        $dars = Dars::where('dourous_id',$dourous->id)->get();
        // $once_dars = Dars::where('dourous_i',$dars->id)->first();
        return view('backoffice.dourous.show',compact('dourous','dars'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
