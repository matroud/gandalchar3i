<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dourous;
use App\Models\Dars;
use Str;
use wapmorgan\Mp3Info\Mp3Info;

class DarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($slug)
    {
        $dourous = Dourous::where('slug', $slug)->first();
        return view('backoffice.dars.create',compact('dourous'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $dourous = Dourous::where('slug',$slug)->first();
         if ($request->file('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $link = Str::slug($request->titre).$dourous->slug.'.'.$file->getClientOriginalExtension();
            $file->move('uploads/dars/', $link);
            $link = '/uploads/dars/'.$link;

        }
         
        $dars = new Dars();
        $dars->title = $request->titre;
        $dars->type_dars = $request->type_dars;
        $dars->dourous_id = $dourous->id;
        $dars->lien_dars = $link;
        // $audio = new Mp3Info($link, true);
        // dd($audio->duration);
        $dars->save();

        return redirect()->route('dourous.show', $dourous->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
