<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chaine;
use App\Models\ChaineUser;
use Auth;
use Str;
class ChaineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $chaines = Chaine::all();
        return view('backoffice.chaines.index',compact('chaines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.chaines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('logo'));
        if ($request->file('logo')) {
            $image = $request->file('logo');
            $link = Str::slug($request->name).'.'.$image->extension();
            $image->move('uploads/chaine/', $link);
            $link = '/uploads/chaine/'.$link;
        } else {
            $link ='/defaultImages/chaine.png';
        }

        $user = Auth::user();

        $chaine = new Chaine();
        $chaine->libelle = $request->name;
        $chaine->email = $request->email;
        $chaine->telephone = $request->telephone;
        $chaine->adresse = $request->adresse;
        $chaine->logo = $link;
        $chaine->statut = Chaine::ACTIF;
        $chaine->created_by = $user->id;
        $chaine->slug = Str::slug($request->name,'-');

        if ($chaine->save()) {

            // flashy()->info('Votre chaine a été crée avec succèss');
            ChaineUser::create([
                'chaine_id' => $chaine->id,
                'user_id' => $user->id
                // 'type' => 'owner'
            ]);
            $user->type = 'organisateur';
            $user->save();
            
            return redirect(route('chaine.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chaine = Chaine::find($id);
        return view('backoffice.chaines.edit',compact('chaine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->file('logo')) {
            $image = $request->file('logo');
            $link = Str::slug($request->name).'.'.$image->extension();
            $image->move('uploads/chaine/', $link);
            $link = '/uploads/chaine/'.$link;
        } else {
            $link ='/defaultImages/chaine.png';
        }
        
        $chaine = Chaine::find($id);
        $chaine->libelle = $request->name;
        $chaine->email = $request->email;
        $chaine->telephone = $request->telephone;
        $chaine->adresse = $request->adresse;
        $chaine->logo = $link;
        $chaine->statut = Chaine::ACTIF;
        $chaine->slug = Str::slug($request->name,'-');
        $chaine->save();
        return redirect(route('chaine.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
