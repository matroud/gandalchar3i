<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Dourous;
use App\Models\Categorie;
use App\Models\Cheick;
class SearchDourous extends Component
{
    use WithPagination;

    public $query;
    public $categorie;
    public $categories;
    public $cheicks;
    public $perPage= 6;
    
    public function mount()
    {
      $this->categories = Categorie::all();
      $this->cheicks = Cheick::all();
    }
    public function render()
    {
        if($this->query)
          $dourous = Dourous::where('title','like','%'. $this->query. '%')->paginate($this->perPage);
        elseif($this->categorie){
           $categorie = Categorie::where('libelle', $this->categorie)->first();
           $dourous = Dourous::where('categorie_id', $categorie->id)->paginate($this->perPage);
        }
        
        else
          $dourous =  Dourous::all();
        return view('livewire.search_dourous',['dourous' => $dourous]);
    }
}
