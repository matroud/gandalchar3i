<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Newsletter;

class NewsletterUser extends Component
{

    public $email;

    public function submit()
    {
        $this->validate([
            'email' => 'required|email'
        ]);

        $newsletter = new Newsletter();
        $newsletter->email = $this->email;
        $newsletter->save();

        session()->flash('message', 'Votre adresse email a été enregistré avec succès.');
        $this->email = '';

    }
    public function render()
    {
        return view('livewire.newsletter_user');
    }
}
