<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Dourous extends Model implements Viewable
{
    use HasFactory;
    use InteractsWithViews;

    protected $guarded = ['id'];

    public function niveau(){return $this->belongsTo('App\Models\Niveau');}
    public function langue(){return $this->belongsTo('App\Models\Langue');}
    public function categorie(){return $this->belongsTo('App\Models\Categorie');}
    public function chaine(){return $this->belongsTo('App\Models\Chaine');}
    public function cheick(){return $this->belongsTo('App\Models\Cheick');}
    public function user(){return $this->belongsTo('App\Models\User', 'created_by');}
    public function dars()
    {
        return $this->hasMany('App\Models\Dars');
    }

}
