<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chaine extends Model
{
    use HasFactory;

    const ACTIF = 'actif';
    const PASSIF = 'passif';
    const STATUT = [Chaine::ACTIF, Chaine::PASSIF];

    protected $guarded = ['id'];

    public function user(){return $this->belongsTo('App\Models\User', 'created_by');}
}
