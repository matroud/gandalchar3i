<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChaineUser extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'chaine_id',
        'type',
    ];
    // protected $hidden=['deleted_at'];
    public function user(){return $this->belongsTo(User::class);}
    public function chaine(){return $this->belongsTo(Chaine::class);}
}
