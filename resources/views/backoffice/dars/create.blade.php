@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Ajouter un dars</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="{{route('dashboard.index')}}">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Ajouter un dars</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header class="text-center"> Ajouter un dars</header>
								</div>
								<div class="card-body">
                                     <h4 class="text-center">Dourous : {{$dourous->title}}</h4>
									<form action="{{route('dars.store', $dourous->slug)}}" method="POST" enctype="multipart/form-data" >
                                        @csrf
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Titre de dars</label>
                                                    <input type="text" class="form-control" id="simpleFormEmail" name="titre" placeholder="Titre de dars">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                        <label for="type_dars">Type de dars</label>
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="radio p-0">
                                                                    <input type="radio" name="type_dars" id="optionsRadios1" value="audio" checked="">
                                                                    <label for="optionsRadios1">
                                                                    Audio
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <div class="radio p-0">
                                                                    <input type="radio" name="type_dars" id="optionsRadios2" value="video">
                                                                    <label for="optionsRadios2">
                                                                        Vidéo
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                               <div class="form-group">
                                                    <label for="simpleFormEmail">Type de fichier </label>
                                                    <div class="row">
                                                        <div class="col-lg-5">
                                                        <div class="radio p-0">
                                                            <input type="radio" name="type_fichier" id="type_fichier" value="upload" checked="">
                                                            <label for="upload">
                                                               Uploader le dars
                                                            </label>
												       </div>
                                                        </div>
                                                        <div class="col-lg-7">
                                                            <div class="radio p-0">
                                                                <input type="radio" name="type_fichier" id="type_fichier" value="link">
                                                                <label for="upload">
                                                                  Mettre Le lien du dars
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Dars </label>
                                                    <input type="file" class="form-control" id="simpleFormEmail" name="file">
                                                </div>
                                                <!-- <div class="form-group" id="link" style="display:none">
                                                    <label for="simpleFormEmail">Lien du dars</label>
                                                    <input type="text" class="form-control" id="simpleFormEmail" name="lien_dars">
                                                </div> -->
                                            </div>

                                       </div>
                                    
                                        <div class="col-lg-12 p-t-20 text-center">
                                            <button type="submit"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Enregister</button>
                                            <button type="button"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Fermer</button>
                                        </div>
                                    </form>
								</div>
							</div>
						</div>
					</div>
				</div>
	</div>
@endsection
