@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Liste des dars</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Liste des dars</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-xl-12">
							<div class="card-box">
								<div class="card-head">
									<header>Liste des dars</header>
								</div>
								<div class="card-body ">
									<!-- <div class="row">
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group">
												<a href="{{route('dourous.create')}}" id="addRow" class="btn btn-info">
													Ajouter un nouveau dars <i class="fa fa-plus"></i>
												</a>
											</div>
										</div>
                                    </div> -->
                                    <div class="row">
                                        @forelse($dourous as $dars)
                                        <div class="col-lg-3 col-md-6 col-12 col-sm-6">
										<div class="blogThumb">
											<div class="thumb-center"><img class="img-responsive" alt="user"
													src="{{$dars->image}}"></div>
											<div class="course-box">
												<h4>{{$dars->title}}</h4>
												<div class="text-muted"><span class="m-r-10">{{ date('d/m/Y', strtotime($dars->created_at))}}</span>
													<a class="course-likes m-l-10" href="#"><i
															class="fa fa-heart-o"></i> 654</a>
												</div>
												<p><span><i class="fa fa-clock-o"></i> Durée: 6 Months</span></p>
                                                <p><span><i class="fa fa-user"></i> Cheick: Jane Doe</span></p>
                                                <p><span><i class="fa fa-graduation-cap"></i> Cours: 200+</span></p>
												<p><span><i class="fa fa-users"></i> Inscris: 200+</span></p>
												<a href="{{ route('dourous.show',['dourou' => $dars->slug]) }}"
													class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">Voir plus ce dars
                                                </a>
													
											</div>
										</div>
									</div>
                                        @empty
                                        @endforelse
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection