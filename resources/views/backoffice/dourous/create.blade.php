@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Ajouter un dars</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="{{route('dashboard.index')}}">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Ajouter un dars</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header class="text-center">Ajouter un dars</header>
								</div>
								<div class="card-body">
									<form action="{{route('dourous.store')}}" method="POST" enctype="multipart/form-data" >
                                        @csrf
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Titre du dars</label>
                                                    <input type="text" class="form-control" id="simpleFormEmail" name="title" placeholder="Titre du dars">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Prerequi</label>
                                                    <input type="text" class="form-control" id="simpleFormEmail" name="prerequis" placeholder="Prerequi du dars">
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                <label for="simpleFormEmail">Catégorie</label>
                                                    <select name="categorie_id" id="" class="form-control">
                                                        @forelse($categories as $categorie)
                                                                <option value="{{$categorie->id}}">{{$categorie->libelle}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                <label for="simpleFormEmail">Niveau</label>
                                                <select name="niveau_id" id="" class="form-control">
                                                        @forelse($niveaux as $niveau)
                                                                <option value="{{$niveau->id}}">{{$niveau->libelle}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                <label for="simpleFormEmail">Langue</label>
                                                    <select name="langue_id" id="" class="form-control">
                                                        @forelse($langues as $langue)
                                                                <option value="{{$langue->id}}">{{$langue->libelle}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                <label for="simpleFormEmail">Chaine</label>
                                                <select name="chaine_id" id="" class="form-control">
                                                        @forelse($chaines as $chaine)
                                                                <option value="{{$chaine->id}}">{{$chaine->libelle}}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                <label for="simpleFormEmail">cheikh chargé du dars</label>
                                                    <select name="cheick_id" id="" class="form-control">
                                                        @forelse($cheicks as $cheick)
                                                                <option value="{{$cheick->id}}">{{$cheick->kouniah }}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Ce dars est-il completé</label>
                                                    <div class="row">
                                                        <div class="offset-lg-1 col-lg-4">
                                                        <div class="radio p-0">
                                                            <input type="radio" name="etat_cours" id="optionsRadios1" value="1" checked="">
                                                            <label for="optionsRadios1">
                                                               Oui
                                                            </label>
												       </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="radio p-0">
                                                                <input type="radio" name="etat_cours" id="optionsRadios2" value="0">
                                                                <label for="optionsRadios2">
                                                                    Non
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Image du dars</label>
                                                    <input type="file" class="form-control" id="simpleFormEmail" name="image" placeholder="Image du dars">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Document pdf du dars</label>
                                                    <input type="file" class="form-control" id="simpleFormEmail" name="document" placeholder="Document pdf du dars">
                                                </div>
                                            </div>
                                       </div>
                                        <div class="col-lg-12 p-t-20 text-center">
                                            <button type="submit"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Enregister</button>
                                            <button type="button"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Fermer</button>
                                        </div>
                                    </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection