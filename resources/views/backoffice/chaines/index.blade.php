@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Liste des chaines</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Liste des chaines</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-xl-12">
							<div class="card-box">
								<div class="card-head">
									<header>Liste des chaines</header>
								</div>
								<div class="card-body ">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group">
												<a href="{{route('chaine.create')}}" id="addRow" class="btn btn-info">
													Ajouter une nouvelle chaine <i class="fa fa-plus"></i>
												</a>
											</div>
										</div>
                                    </div>
                                    <div class="row">
                                    @forelse($chaines as $chaine)
                                        <div class="col-md-4">
                                            <div class="profile-sidebar">
                                                <div class="card card-topline-aqua">
                                                    <div class="card-head card-topline-aqua">
                                                        <header class="text-center">{{$chaine->libelle}}</header>
                                                    </div>
                                                    <div class="card-body no-padding height-9">
                                                        <div class="row">
                                                            <!-- <div class="profile-usertitle text-center">
                                                                <div class="profile-usertitle-name"> {{$chaine->libelle}} </div>
                                                            </div> -->
                                                            <div class="course-picture">
                                                                <img src="{{$chaine->logo}}"  style="height:100px" class="ml-5 img-responsive"
                                                                    alt=""> 
                                                            </div>
                                                        </div>
                                                        <ul class="list-group list-group-unbordered">
                                                            <li class="list-group-item">
                                                                <b>Téléphone </b>
                                                                <div class="profile-desc-item pull-right">{{$chaine->telephone}}</div>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <b>Adresse E-mail </b>
                                                                <div class="profile-desc-item pull-right">{{ $chaine->email}}</div>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <b>Propriétaire</b>
                                                                <div class="profile-desc-item pull-right">{{ $chaine->user->full_name()}}</div>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <b>Date de création</b>
                                                                <div class="profile-desc-item pull-right">{{ date('d/m/Y', strtotime($chaine->created_at))}}</div>
                                                            </li>
										                </ul>
                                                        <!-- END SIDEBAR USER TITLE -->
                                                        <div class="col-lg-12 p-t-20 text-center">
                                                                <a href="{{route('chaine.edit',$chaine->id)}}"
                                                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-success">Modifier</a>
                                                                <a href="" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-danger">Supprimer</a>
                                                        </div>
                                                    </div>
                                                </div>
                                    
                                            </div>
                                        </div>
                                    @empty
                                     @endforelse
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection