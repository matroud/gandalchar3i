@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Modification d'une chaine</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="{{route('dashboard.index')}}">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Modification d'une chaine</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card-box">
								<div class="card-head">
									<header class="text-center">Modification d'une chaine</header>
								</div>
								<div class="card-body">
									<form action="{{route('chaine.update',$chaine->id)}}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')  
                                    @csrf
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Nom de la chaine</label>
                                                    <input type="text" class="form-control" id="simpleFormEmail" value="{{ $chaine->libelle }}" name="name" placeholder="Nom de la chaine">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Logo</label>
                                                    <input type="file" class="form-control" id="simpleFormEmail" value="{{ $chaine->photo }}" name="logo"  placeholder="logo">
                                                </div>
                                            </div>
                                       </div>
                                       <div class="row">
                                            <div class="offset-lg-1 col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Téléphone</label>
                                                    <input type="tel" class="form-control" id="simpleFormEmail" value="{{ $chaine->telephone }}" name="telephone" placeholder="Téléphone">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 p-t-20">
                                                <div class="form-group">
                                                    <label for="simpleFormEmail">Adresse E-mail</label>
                                                    <input type="email" class="form-control" id="simpleFormEmail" value="{{ $chaine->email }}" name="email" placeholder="Adresse E-mail">
                                                </div>
                                            </div>
                                       </div>
                                    
                                        <div class="col-lg-12 p-t-20 text-center">
                                            <button type="submit"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Enregister</button>
                                            <button type="button"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Fermer</button>
                                        </div>
                                    </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection