<div class="sidebar-container">
				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
					<div id="remove-scroll" class="left-sidemenu">
						<ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false"
							data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
							<li class="sidebar-toggler-wrapper hide">
								<div class="sidebar-toggler">
									<span></span>
								</div>
							</li>
							<li class="sidebar-user-panel">
								<div class="user-panel">
									<div class="pull-left image">
										<img src="/backoffice/assets/img/avatar.png" class="img-circle user-img-circle"
											alt="User Image" />
									</div>
									<div class="pull-left info">
										<p>  {{Auth::user()->prenom}}</p>
										<a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline">
												Online</span></a>
									</div>
								</div>
							</li>
							<!-- <li class="nav-item">
								<a href="event.html" class="nav-link nav-toggle"> <i class="material-icons">event</i>
									<span class="title">Event Management</span>
								</a>
							</li> -->
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"> <i class="material-icons">local_library</i>
									<span class="title">Chaine</span> <span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="{{route('chaine.index')}}" class="nav-link "> <span class="title">Liste des chaines</span>
										</a>
									</li>
									<li class="nav-item">
										<a href="{{route('chaine.create')}}" class="nav-link "> <span class="title">Ajouter une chaine</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"> <i class="material-icons">person</i>
									<span class="title">Cheickh</span> <span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="{{route('cheick.index')}}" class="nav-link "> <span class="title">Liste des cheickh</span>
										</a>
									</li>
									<li class="nav-item">
										<a href="{{route('cheick.create')}}" class="nav-link "> <span class="title">Ajouter un nouveau cheickh</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"> <i class="material-icons">school</i>
									<span class="title">Dourous</span> <span class="arrow"></span>
									<!-- <span class="label label-rouded label-menu label-success">new</span> -->
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="{{route('dourous.index')}}" class="nav-link "> <span class="title">Liste des dourous
												</span>
										</a>
									</li>
									<li class="nav-item">
										<a href="{{route('dourous.create')}}" class="nav-link "> <span class="title">Ajouter un nouveau dars</span>
										</a>
									</li>
									
								</ul>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"><i class="material-icons">group</i>
									<span class="title">Apprenants</span><span class="arrow"></span></a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="all_students.html" class="nav-link "> <span class="title">Liste des apprenants
												</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
									<span class="title">Administration</span> <span class="arrow"></span>
									<!-- <span class="label label-rouded label-menu label-success">new</span> -->
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="{{route('langue.index')}}" class="nav-link "> <span class="title">Langues
												</span>
										</a>
									</li>
									<li class="nav-item">
										<a href="{{route('categorie.index')}}" class="nav-link "> <span class="title">Catégorie</span>
										</a>
									</li>
									<li class="nav-item">
										<a href="{{route('niveau.index')}}" class="nav-link "> <span class="title">Niveau</span>
										</a>
									</li>
									
								</ul>
							</li>
							<li class="nav-item">
								<a href="{{route('root_index')}}" class="nav-link nav-toggle"> <i class="material-icons">event</i>
									<span class="title">Retour sur le site</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>