<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="SmartUniversity" />
	<title>Gandal char3i</title>
	<!-- google font -->
	<link href="../../../../../../fonts.googleapis.com/css6079.css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="/backoffice/fonts/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/fonts/material-design-icons/material-icon.css" rel="stylesheet" type="text/css" />
	<!--bootstrap -->
	<link href="/backoffice/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/assets/plugins/summernote/summernote.css" rel="stylesheet">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="/backoffice/assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="/backoffice/assets/css/material_style.css">
	<!-- inbox style -->
	<link href="/backoffice/assets/css/pages/inbox.min.css" rel="stylesheet" type="text/css" />
	<!-- data tables -->
	<link href="/backoffice/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet"
		type="text/css" />
	<link href="/backoffice/assets/plugins/summernote/summernote.css" rel="stylesheet">
	<!-- Theme Styles -->
	<link href="/backoffice/assets/css/theme/light/theme_style.css" rel="stylesheet" id="rt_style_components" type="text/css" />
	<link href="/backoffice/assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/assets/css/theme/light/style.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="/backoffice/assets/css/theme/light/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
	<link rel="shortcut icon" href="/backoffice/assets/img/favicon.ico" />
</head>
<!-- END HEAD -->

<body
	class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-indigo">
	<div class="page-wrapper">
		<!-- start header -->
		<div class="page-header navbar navbar-fixed-top">
			<div class="page-header-inner ">
				<!-- logo start -->
				<div class="page-logo">
					<a href="index.html">
						<!-- <span class="logo-icon material-icons fa-rotate-45">Gandal Char3i</span> -->
						<span class="logo-default">Gandal Char3i</span>
					 </a>
				</div>
				<!-- logo end -->
				<ul class="nav navbar-nav navbar-left in">
					<li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
				</ul>
				<form class="search-form-opened" action="#" method="GET">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search..." name="query">
						<span class="input-group-btn">
							<a href="javascript:;" class="btn submit">
								<i class="icon-magnifier"></i>
							</a>
						</span>
					</div>
				</form>
				<!-- start mobile menu -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span></span>
				</a>
				<!-- end mobile menu -->
				<!-- start header menu -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<li><a href="javascript:;" class="fullscreen-btn"><i class="fa fa-arrows-alt"></i></a></li>
						<li class="dropdown dropdown-user">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
								data-close-others="true">
								<img alt="" class="img-circle " src="/backoffice/assets/img/dp.jpg" />
								<span class="username username-hide-on-mobile"> {{Auth::user()->nom}} {{ Auth::user()->prenom}} </span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-default">
								<li>
									<a href="user_profile.html">
										<i class="fa fa-spin fa-cog"></i> Tableau de bord </a>
								</li>
								<li>
									<a href="#">
										<i class="icon-settings"></i> Mes paramètres
									</a>
								</li>
								<li class="divider"> </li>
								<li>
									<a href="login.html">
										<i class="icon-logout"></i> Déconnexion </a>
								</li>
							</ul>
						</li>
						<!-- end manage user dropdown -->
					</ul>
				</div>
			</div>
		</div>
		<!-- start page container -->
		<div class="page-container">
		  @include('backoffice.layouts.sidebar')
		  @yield('content')
		</div>
		<!-- end page container -->
		<!-- start footer -->
		<div class="page-footer">
			<div class="page-footer-inner"> {{Date('Y')}} &copy; Gandal Char3i
				<a href="mailto:redstartheme@gmail.com" target="_top" class="makerCss">Gandal Char3i</a>
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- end footer -->
	</div>
	<!-- start js include path -->
	<script src="/backoffice/assets/plugins/jquery/jquery.min.js"></script>
	<script src="/backoffice/assets/plugins/popper/popper.js"></script>
	<script src="/backoffice/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
	<script src="/backoffice/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	<!-- bootstrap -->
	<script src="/backoffice/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/backoffice/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="/backoffice/assets/plugins/sparkline/jquery.sparkline.js"></script>
	<script src="/backoffice/assets/js/pages/sparkline/sparkline-data.js"></script>
	<!-- Common js-->
	<script src="/backoffice/assets/js/app.js"></script>
	<script src="/backoffice/assets/js/layout.js"></script>
	<script src="/backoffice/assets/js/theme-color.js"></script>
	<!-- material -->
	<script src="/backoffice/assets/plugins/material/material.min.js"></script>
	<!--apex chart-->
	<script src="/backoffice/assets/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="/backoffice/assets/js/pages/chart/chartjs/home-data.js"></script>
	<!-- summernote -->
	<script src="/backoffice/assets/plugins/summernote/summernote.js"></script>
	<script src="/backoffice/assets/js/pages/summernote/summernote-data.js"></script>
	<!-- end js include path -->
    <!-- Data table -->
	<script src="/backoffice/assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/backoffice/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
	<script src="/backoffice/assets/js/pages/table/table_data.js"></script>

		<!-- summernote -->
	<script src="/backoffice/assets/plugins/summernote/summernote.js"></script>
	<script src="/backoffice/assets/js/pages/summernote/summernote-data.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/turbolinks@5.2.0/dist/turbolinks.min.js"></script>

	<!-- @yield('extrajs') -->
</body>

</html>