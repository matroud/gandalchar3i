@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Liste des cheicks</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Liste des cheicks</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-xl-12">
							<div class="card-box">
								<div class="card-head">
									<header>Liste des cheicks</header>
								</div>
								<div class="card-body ">
									<!-- <div class="row">
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group">
												<a href="{{route('cheick.create')}}" id="addRow" class="btn btn-info">
													Ajouter un nouveau cheick <i class="fa fa-plus"></i>
												</a>
											</div>
										</div>
                                    </div> -->
                                    <div class="row">
                                    @forelse($cheicks as $cheick)
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-body no-padding height-9">
                                                    <div class="row">
                                                        <div class="profile-userpic">
                                                                <img src="/backoffice/assets/img/avatar.png" class="img-responsive" alt=""> </div>
                                                        </div>
                                                    <div class="profile-usertitle">
                                                        <div class="profile-usertitle-name"> {{$cheick->nom}} {{$cheick->prenom}} </div>
                                                        <div class="profile-usertitle-job"> {{$cheick->kouniah}} </div>
                                                    </div>
                                                    <div class="profile-desc">
                                                        {{$cheick->parcours}} 
                                                    </div>
                                                    <ul class="list-group list-group-unbordered">
                                                        <li class="list-group-item">
                                                            <b>Téléphone </b>
                                                            <div class="profile-desc-item pull-right">{{$cheick->telephone}} </div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Adresse E-mail </b>
                                                            <div class="profile-desc-item pull-right">{{$cheick->email}}</div>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Fonction </b>
                                                            <div class="profile-desc-item pull-right">{{$cheick->fonction}}</div>
                                                        </li>
                                                    </ul>
                                                    <div class="row list-separated profile-stat">
                                                        <div class="col-md-6 col-sm-4 col-6">
                                                            <div class="uppercase profile-stat-title"> 37 </div>
                                                            <div class="uppercase profile-stat-text"> Cours donnés </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-4 col-6">
                                                            <div class="uppercase profile-stat-title"> 151 </div>
                                                            <div class="uppercase profile-stat-text"> Etudiants </div>
                                                        </div>

                                                    </div>
                                                    <div class="profile-userbuttons">
                                                        <button type="button"
                                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-success">Modifier</button>
                                                        <button type="button"
                                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-info">Voir ses dourous</button>
                                                                <button type="button"
                                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-danger">Bloquer ce cheick</button>
										             </div>
                                                </div>
                                    </div>

                                    </div>
                                    @empty
                                     @endforelse
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection