@extends('backoffice.layouts.app')
@section('content')
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Langue</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Liste des catégories</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-xl-12">
							<div class="card-box">
								<div class="card-body ">
                                <div class="row">
								    <div class="col-md-6 col-sm-6 col-6">
										<div class="btn-group">
												<a href="{{route('categorie.create')}}" id="addRow" class="btn btn-info">
													Ajouter une catégorie <i class="fa fa-plus"></i>
												</a>
										</div>
									</div>
								</div>
									<div class="table-scrollable">
										<table
											class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"
											id="example4">
											<thead>
												<tr>
													<th>#</th>
													<th class="text-center">Libelle de la catégorie</th>
													<th class="text-center">Action</th>
												</tr>
											</thead>
											<tbody>
											@forelse($categories as $categorie)
                                            <tr class="even">
													<td>{{$categorie->id}}</td>
													<td class="text-center">{{$categorie->libelle}}</td>
													<td class="text-center"><a href="javascript:void(0)" class="" data-toggle="tooltip"
															title="Edit">
                                                            <i class="fa fa-check"></i></a> 
                                                            <a href="javascript:void(0)"
															class="text-inverse" title="Delete" data-toggle="tooltip">
															<i class="fa fa-trash text-danger"></i></a>
													</td>
												</tr>
                                            @empty
                                            @endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection