@extends('layouts.app')
@section('content')
<!-- <div class="preloader">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div> -->
        <!-- end preloader -->
        <!-- Start header -->
        <!-- end of header -->
        <!-- start of hero -->
        <section class="hero hero-style-3">
            <div class="hero-slider">
                <div class="slide">
                    <div class="container">
                        <div class="row">
                            <div class="col col-lg-8 col-md-7 slide-caption">
                                <div class="slide-top">
                                    <span>Apprenons la science legiferée</span>
                                </div>
                                <div class="slide-title">
                                    <h2>Certes, cette science est une religion ; Regardez donc de qui vous prenez votre religion</h2>
                                </div>
                                <div class="slide-subtitle">
                                    <p>Nous mettons à votre disposition des cours réligieux sur le tawhid, Aquidah, Manhaj, figh, tajwid, langue arabe etc.. dans nos langues nationales.Ces cours seront dispensés par des frères qui ont une croyance authentique(une croyance basée sur le coran et la sounnah selon la compréhension des Salafs Salihs (les pieux prédécesseurs)) </p>
                                </div>
                                <!-- <div class="btns">
                                    <a href="about.html" class="theme-btn">Discover More</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="right-vec">
                        <img src="/assets/images/slider/img-2.png" alt="">
                        <div class="right-border">
                            <div class="right-icon"><i class="fi flaticon-quran"></i></div>
                            <div class="right-icon"><i class="fi flaticon-taj-mahal-1"></i></div>
                            <div class="right-icon"><i class="fi flaticon-allah-word"></i></div>
                            <div class="right-icon"><i class="fi flaticon-muhammad-word"></i></div>
                            <div class="right-icon"><i class="fi flaticon-prayer"></i></div>
                            <div class="right-icon"><i class="fi flaticon-business-and-finance"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="service-area">
            <div class="container">
                <div class="service-wrap">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="service-item">
                                <div class="service-title">
                                    <span>Calendrier</span>
                                    <h2>Le programme de la semaine</h2>
                                    <!-- <p>The Weekend School of the Islamic Center of Allah is committed to providing quality Islamic Education according to the Quran and the Sunnah of the Prophet Mohammed (Peace Be upon Him) to a diverse student population in the greater area.</p> -->
                                </div>
                                <div class="routine-table">
                                    <ul class="routne-head">
                                        <li>Evénement</li>
                                        <li>Date/heure</li>
                                        <li>Lieu</li>
                                        <li>Intervenant</li>
                                        <li>Langue</li>
                                    </ul>
                                    <ul>
                                        <li>Cours oussoul thalatha</li>
                                        <li>12/02/2021 à 15h30</li>
                                        <li>En ligne</li>
                                        <li>Abou Amina</li>
                                        <li>Soussou</li>
                                    </ul>
                                    <ul>
                                        <li>Cours nawaqadoul-islam </li>
                                        <li>12/06/2021 à 17h30</li>
                                        <li>Mosquéée salafi de Yimbaya</li>
                                        <li>Ousmane Diallo</li>
                                        <li>Malinké</li>
                                    </ul>
                                    <ul>
                                        <li>Cours Charhous-sounnah</li>
                                        <li>12/03/2021 à 18h00</li>
                                        <li>Dixinn</li>
                                        <li>Abou fatima Kallo</li>
                                        <li>Poular</li>
                                    </ul>
                                </div>
                                <!-- <div class="btns">
                                    <a href="service-single.html" class="theme-btn-s3">Know More</a>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-4">
                            <div class="service-img">
                                <img src="/assets/images/service/img-2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of hero slider -->
        <!-- wpo-about-area start -->
        <div class="wpo-about-area section-padding">
            <div class="container">
                <div class="wpo-about-wrap">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="wpo-about-img-3">
                                <img src="/assets/images/about3.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 colsm-12">
                            <div class="wpo-about-text">
                                <div class="wpo-section-title">
                                    <span>Qui sommes-nous ? </span>
                                    <h2>بسم الله الرحمن الرحيم </h2>
                                    <h5 class="text-center">Au nom d'Allah, le Tout Miséricordieux, le Très Miséricordieux</h5>
                                </div>
                                <p>Toute la louange appartient à Allah, nous Le louons, nous Lui demandons son aide, et nous cherchons refuge en Allah contre le mal qui est en nous-mêmes et contre les mauvaises conséquences de nos mauvaises actions, celui qu’Allah guide, personne ne peut l’égarer et celui qu’Allah égare personne peut le guider. Je témoigne qu’il n’existe rien qui mérite d’être adoré excepté Allah, Lui seul et Il n’a aucun associé et je témoigne que Mohammad est son esclave et messager. 
                                    <br>Ensuite : <br>
                                    Ce site a été créé dans le cadre : <br>
                                    1 - La Da’wah au Tawhid et le combat contre le Shirk sous toute ses formes, par le moyen de l’éducation, de l’enseignement et de la diffusion du savoir. <br>
                                    2 - La Da’wah à la Sounnah et le combat des Bid’ahs sous toutes leurs formes, par le moyen de l’éducation, de l’enseignement et de la diffusion du savoir. <br>
                                    3 - Organiser des séminaires et des cours sur l’Islam avec les savants Salafis et avec les étudiants en science Islamique pour donner des bases Islamiques et scientifiques solides à cette Da’wah.
                                </p>
                                <!-- <div class="btns">
                                    <a href="about.html" class="theme-btn" tabindex="0">Discover More</a>
                                    <ul>
                                        <li class="video-holder">
                                            <a href="https://www.youtube.com/embed/LTqRm53QjI0" class="video-btn" data-type="iframe" tabindex="0"></a>
                                        </li>
                                        <li class="video-text">
                                            <a href="https://www.youtube.com/embed/LTqRm53QjI0" class="video-btn" data-type="iframe" tabindex="0">
                                                Watch Our Video
                                            </a>
                                        </li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timer-section">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="timer-text">
                                <h2>Les horaires de la prière</h2>
                                <!-- <p>Prayer times in United Arab Emirates</p> -->
                                <span>{{date('d/m/Y')}}</span>
                                
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <div class="timer-num">
                                <ul>
                                    <li>Fajr<span id="fajr"></span></li>
                                    <li>Sunrize<span id="sunrise"></span></li>
                                    <li>Dhuhr<span id="dhuhr"></span></li>
                                    <li>Asr<span id="asr"></span></li>
                                    <li>Maghrib<span id="maghrib"></span></li>
                                    <li>Isha'a<span id="isha"></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="timer-shape1">
                        <img src="/assets/images/prayer-shape/2.png" alt="">
                    </div>
                    <div class="timer-shape2">
                        <img src="/assets/images/prayer-shape/1.png" alt="">
                    </div>
                    <div class="timer-shape3">
                        <img src="/assets/images/prayer-shape/3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-about-area end -->

       <!-- courses-area start -->
       <!-- <div class="courses-area">
           <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>Top Courses</span>
                            <h2>Our Populer Courses</h2>
                        </div>
                    </div>
                </div>
               <div class="row">
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon1.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Quran Memorization</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon2.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Junior Preschool</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon3.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Junior High School</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon4.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Islamic Education</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon5.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Arabic Language</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4 col-sm-6 custom-grid col-12">
                       <div class="courses-item">
                           <div class="course-icon">
                               <span><img src="/assets/images/course/icon6.png" alt=""></span>
                           </div>
                           <div class="course-text">
                               <h2>Learning Technologies</h2>
                               <p>This course intends to help the students memorize Holy Quran. Students who wish to memorize a part or all the Quran.</p>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> -->
       <!-- courses-area start -->
        <!-- wpo-event-area start -->
        <div class="wpo-event-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>Nos dernières publications</span>
                            <h2>Articles</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12 custom-grid">
                        <div class="wpo-event-item">
                            <div class="wpo-event-img">
                                <img src="/assets/images/event/img-1.jpg" alt="">
                                <div class="thumb-text">
                                    <span>25</span>
                                    <span>NOV</span>
                                </div>
                            </div>
                            <div class="wpo-event-text">
                                <h2>Learn About Hajj</h2>
                                <ul>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 - 5.00</li>
                                    <li><i class="fi ti-location-pin"></i>Newyork City</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <a href="event-single.html">Learn More...</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12 custom-grid">
                        <div class="wpo-event-item">
                            <div class="wpo-event-img">
                                <img src="/assets/images/event/img-2.jpg" alt="">
                                <div class="thumb-text-2">
                                    <span>25</span>
                                    <span>NOV</span>
                                </div>
                            </div>
                            <div class="wpo-event-text">
                                <h2>Islamic Teaching Event</h2>
                                <ul>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>8.00 - 5.00</li>
                                    <li><i class="fi ti-location-pin"></i>Newyork City</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                <a href="event-single.html">Learn More...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-event-area end -->
        <!-- support-area start -->
        <div class="support-area-3 section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="support-text">
                            <span>Statistiques du site</span>
                            <h2>Nous avons sur ce site </h2>
                            <p>Gandal-char3i s'est engagée à vous fournir une éducation islamique de qualité axée sur le Coran et la sounnah selon la compréhension des salafs salihs(les pieux prédécesseurs) .</p>
                            <!-- <div class="btns">
                                <a href="donate.html" class="theme-btn-s3">Donate Now</a>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="wpo-counter-grids">
                            <div class="grid">
                                <div class="counter-number">
                                    <h2><span class="odometer" data-count="500">00</span>+</h2>
                                </div>
                                <p>Dourous</p>
                            </div>
                            <div class="grid">
                                <div class="counter-number">
                                    <h2><span class="odometer" data-count="900">00</span>+</h2>
                                </div>
                                <p>Conférences</p>
                            </div>
                            <div class="grid">
                                <div class="counter-number">
                                    <h2><span class="odometer" data-count="30">00</span>+</h2>
                                </div>
                                <p>Intervenants</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- support-area start -->
        <!-- blog-area start -->
        <div class="blog-area-2" style="margin-top:50px">
            <!-- <div class="container">
                <div class="col-l2">
                    <div class="wpo-section-title">
                        <span>From Our Blog</span>
                        <h2>Latest News</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12 custom-grid">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="/assets/images/blog/img-1.jpg" alt="">
                            </div>
                            <div class="blog-content">
                                <h3><a href="blog-single.html">The Importance of Marriage in Islam.</a></h3>
                                <ul class="post-meta">
                                    <li><img src="/assets/images/blog/admin.jpg" alt=""></li>
                                    <li><a href="#">Jenefar Jany</a></li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 21 Jan 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 custom-grid">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="/assets/images/blog/img-2.jpg" alt="">
                            </div>
                            <div class="blog-content">
                                <h3><a href="blog-single.html">Salat is the best exercise for body fitness</a></h3>
                                <ul class="post-meta">
                                    <li><img src="/assets/images/blog/admin.jpg" alt=""></li>
                                    <li><a href="#">Jenefar Jany</a></li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 21 Jan 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 custom-grid">
                        <div class="blog-item b-0">
                            <div class="blog-img">
                                <img src="/assets/images/blog/img-3.jpg" alt="">
                            </div>
                            <div class="blog-content">
                                <h3><a href="blog-single.html">Salat is the best exercise for body fitness</a></h3>
                                <ul class="post-meta">
                                    <li><img src="/assets/images/blog/admin.jpg" alt=""></li>
                                    <li><a href="#">Jenefar Jany</a></li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> 21 Jan 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- blog-area start -->
        <!-- Popup Modal -->
<div id="popModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Hadith du jour</h4>
      </div>
      <div class="modal-body">
        <p>D'après Abou Darda (qu'Allah l'agrée), le Prophète (que la prière d'Allah et Son salut soient sur lui) a dit: « Celui qui prend un chemin pour rechercher la science, Allah lui fait prendre par cela un chemin vers le paradis.
            Certes les anges étendent leurs ailes pour celui qui recherche la science.
            Certes tous ceux qui sont dans les cieux et sur la terre demandent le pardon pour le savant même le poisson dans l'eau.
            Le mérite du savant sur l'adorateur est comme le mérite de la lune par rapport aux autres astres.
            Certes les savants sont les héritiers des prophètes et les prophètes n'ont pas laissé comme héritage des dinars ou des dirhams mais ils ont laissé la science, celui qui la prend aura prit une part importante ».
            (Rapporté par Tirmidhi dans ses Sounan n°2682 et authentifié par cheikh Albani dans sa correction de Sounan Tirmidhi)
        </p>
      </div>
    </div>
 
  </div>
</div>
@endsection
@section('extrajs')
<script>
  $(document).ready(function() {
     if (sessionStorage.getItem('#popModal') !== 'true') {
         setTimeout(function(){
        // open popup after 5 seconds
           $("#popModal").modal('show');
        },5000);
         sessionStorage.setItem('#popModal','true');     
     }
 })
</script>
@endsection