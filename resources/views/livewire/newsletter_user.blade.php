<section class="wpo-news-letter-section">
                <div class="container">
                    <div class="wpo-news-letter-wrap">
                        <div class="row">
                            <div class="col col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2">
                                <div class="wpo-newsletter">
                                    <h3>Suivez-nous pour plus d'informations</h3>
                                    <p>Je souhaite être informé(e) des nouveaux dourous et Conférences près de chez moi.</p>
                                    <div class="wpo-newsletter-form">
                                        @if (session()->has('message'))
                                            <div class="alert alert-success text-center">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <form  method="POST" wire:submit.prevent="submit">
                                            <div>
                                                <input type="text" placeholder="Entrer votre adresse E-mail" wire:model="email" class="form-control">
                                                <button type="submit">Valider</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
</section>
