<header id="header" class="wpo-site-header wpo-header-style-3">
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{route('root_index')}}"><img src="/assets/images/logo.png" alt="logo"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="ti-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li><a href="{{route('dourous')}}">Dourous</a></li>
                            <li><a href="#">Conférences</a></li>
                            <li><a href="#">Sermons</a></li>
                            <li><a href="{{route('coran')}}">Coran</a></li>
                            <li><a href="#">Le direct</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                    <div class="cart-search-contact">
                        <div class="btns">
                            <a href="contact.html" class="theme-btn">Contactez-nous</a>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>