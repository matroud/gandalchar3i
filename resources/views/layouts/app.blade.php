<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="wpoceans">
    <title>Gandal-char3i</title>
    <link href="/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/flaticon.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="/assets/css/owl.theme.css" rel="stylesheet">
    <link href="/assets/css/slick.css" rel="stylesheet">
    <link href="/assets/css/slick-theme.css" rel="stylesheet">
    <link href="/assets/css/swiper.min.css" rel="stylesheet">
    <link href="/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/assets/css/odometer-theme-default.css" rel="stylesheet">
    <link href="/assets/css/nice-select.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    @yield('extracss')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @livewireStyles
</head>

<body>
    <!-- start page-wrapper -->
    <div class="page-wrapper">
        <!-- start preloader -->
          @include('layouts.navbar')
          @yield('content')

        <!-- footer-area start -->
        <div class="wpo-ne-footer-2">
            <!-- start wpo-news-letter-section -->
            <livewire:newsletter-user />
            <!-- end wpo-news-letter-section -->
            <!-- start wpo-site-footer -->
            <footer class="wpo-site-footer">
                <div class="wpo-upper-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col col-lg-5 col-md-3 col-sm-6">
                                <div class="widget about-widget">
                                    <div class="logo widget-title">
                                        <img src="/assets/images/logo.png" alt="blog">
                                    </div>
                                    <p>Gandal char3i est la 1ere plateforme d'apprentissage de la science religieuse en guinée.Voulez-vous apprendre la science legiferée en ligne?
                                     Vous pouvez compter sur Gandal Char3i pour étudier differents dars religieux. Nous proposons une large gamme de dars sur le tawhid, l'aquidah, le fiqh, 
                                     la langue arabe, le tajwid etc.. dispensés par nos frères guinéens  sunnites. Gandal Char3i est la meilleure destination disponible.
                                     </p>
                                    <ul>
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                                        <li><a href="#"><i class="ti-google"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- <div class="col col-lg-3 col-md-3 col-sm-6">
                                <div class="widget link-widget">
                                    <div class="widget-title">
                                        <h3>Service</h3>
                                    </div>
                                    <ul>
                                        <li><a href="service-single.html">Islamic School</a></li>
                                        <li><a href="service-single.html">Our Causes</a></li>
                                        <li><a href="#">Our Mission</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                        <li><a href="event.html">Our Event</a></li>
                                    </ul>
                                </div>
                            </div> -->
                            <div class="col col-lg-2 col-md-3 col-sm-6">
                                <div class="widget link-widget">
                                    <div class="widget-title">
                                        <h3>Autres liens</h3>
                                    </div>
                                    <ul>
                                        <!-- <li><a href="#">Qui sommes-nous</a></li> -->
                                        <li><a href="s#">Réfutation
                                        </a></li>
                                        <li><a href="#">Articles</a></li>
                                        <li><a href="#">Fatawas</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-lg-3 col-lg-offset-1 col-md-3 col-sm-6">
                                <div class="widget market-widget wpo-service-link-widget">
                                    <div class="widget-title">
                                        <h3>Contact </h3>
                                    </div>
                                    <p>online store with lots of cool and exclusive wpo-features</p>
                                    <div class="contact-ft">
                                        <ul>
                                            <li><i class="fi ti-location-pin"></i>Madina centre </li>
                                            <li><i class="fi flaticon-call"></i>+224 626 336 494</li>
                                            <li><i class="fi flaticon-envelope"></i>gandal-char3i@gmail.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="wpo-lower-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col col-xs-12">
                              <p class="copyright">&copy; {{Date('Y')}} Gandal-char3i.Tous les droits réservés</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end wpo-site-footer -->
        </div>
    </div>
    <!-- end of page-wrapper -->
    <!-- All JavaScript files
    ================================================== -->
    @livewireScripts
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/circle-progress.min.js"></script>
    <!-- Plugins for this template -->
    <script src="/assets/js/jquery-plugin-collection.js"></script>
    <!-- Custom script for this template -->
    <script src="/assets/js/adhan.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.28/moment-timezone-with-data-10-year-range.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/turbolinks@5.2.0/dist/turbolinks.min.js"></script>
    <script src="/assets/js/script.js"></script>
    @yield('extrajs')
    <script>
        var date = new Date();
        var coordinates = new adhan.Coordinates(9.53795, -13.67729);
        var params = adhan.CalculationMethod.MuslimWorldLeague();
        params.madhab = adhan.Madhab.Hanafi;
        var prayerTimes = new adhan.PrayerTimes(coordinates, date, params);

        var fajrTime = moment(prayerTimes.fajr).tz('Guinea/Conakry').format('H:mm A');
        var sunriseTime = moment(prayerTimes.sunrise).tz('Guinea/Conakry').format('H:mm ');
        var dhuhrTime = moment(prayerTimes.dhuhr).tz('Guinea/Conakry').format('H:mm A');
        var asrTime = moment(prayerTimes.asr).tz('Guinea/Conakry').format('H:mm A');
        var maghribTime = moment(prayerTimes.maghrib).tz('Guinea/Conakry').format('H:mm A');
        var ishaTime = moment(prayerTimes.isha).tz('Guinea/Conakry').format('H:mm A');

        var current = moment().format("hh:mm");
        if(fajrTime == current || sunriseTime == current || dhuhrTime == current || asrTime == current || maghribTime == current|| ishaTime == current ){
            var audio = new Audio('http://praytimes.org/audio/adhan/Sunni/Adhan%20Madina.mp3');
           audio.play();
        }
      
       

        //show
        document.getElementById('fajr').innerHTML = fajrTime;
        document.getElementById('sunrise').innerHTML = sunriseTime;
        document.getElementById('dhuhr').innerHTML = dhuhrTime;
        document.getElementById('asr').innerHTML = asrTime;
        document.getElementById('maghrib').innerHTML = maghribTime;
        document.getElementById('isha').innerHTML = ishaTime;

    </script>
</body>

</html>