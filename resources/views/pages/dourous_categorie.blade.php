@extends('layouts.app')
@section('content')
<div class="wpo-event-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title" style="margin-bottom:30px">
                            <!-- <span>Our Events</span> -->
                            <h2>Nos dourous</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-2 col col-md-8">
                            <div class="wpo-blog-sidebar">
                                <div class="widget search-widget">
                                    <form>
                                        <div>
                                            <input type="text" class="form-control" placeholder="Rechercher un dars">
                                            <button type="submit"><i class="ti-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                   <div class="form-group">
                                        <label>Trier Par Thèmes</label>
                                        <select class="form-control">
                                            <option>Tawhid</option>
                                            <option>Figh</option>
                                            <option>Aquidah</option>
                                        </select>
									</div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                   <div class="form-group">
                                        <label>Trier Par cheicks</label>
                                        <select class="form-control">
                                            <option>Abou Fatima kallo</option>
                                            <option>Abou fawzan Diallo</option>
                                            <option>Abou ibrahima Sow</option>
                                        </select>
									</div>
                                </div>
                            </div>
                    </div>
                </div>
               <div class="panel panel-default">
                   <div class="panel-body">
                        <div class="row">
                          @forelse($dourous as $dars)
                          <div class="col-md-6 col-sm-12 col-lg-6 col-12 custom-grid">
                            <div class="wpo-event-item">
                                <div class="wpo-event-img">
                                    <img src="{{$dars->image}}" style="width:240px;height:260px" alt="">
                                    <div class="thumb-text">
                                        <span>dars</span>
                                        <span>fini</span>
                                    </div>
                                </div>
                                <div class="wpo-event-text">
                                    <h2>{{$dars->title}}</h2>
                                    <ul>
                                        <li><i class="fi ti-stats-up" aria-hidden="true"></i>{{$dars->niveau->libelle}}</li>
                                        <li><i class="fi ti-microphone"></i>{{$dars->langue->libelle}}</li>
                                    </ul>
                                    <ul>
                                      <li class="text-center"><i class="fi ti-book"></i>Catégorie: <span class="text-info">{{$dars->categorie->libelle}}</span></li>
                                      <li class="text-center"><i class="fi ti-id-badge"></i>Organisé: <span class="text-primary">{{$dars->chaine->libelle}}</span></li>
                                    </ul>
                                     <ul> 
                                       <li>  <i class="fi ti-book"></i>Nombre de dars ajouté(s) : {{count($dars->dars)}}</li>
                                     </ul>
                                    <ul>
                                    <li class="text-center">
                                      <i class="fi ti-user"></i>Intervenant: <span class="text-success">{{$dars->cheick->kouniah}} </span> <br> (qu'Allah le préserve )</li>
                                    </ul>

                                    <a href="{{ route('dars',['slug_dars' => $dars->slug])}}"> <i class="fi ti-unlock"></i>Suivre ce dars</a>
                                </div>
                            </div>
                          </div>
                          @empty
                          <div class="container">
                               <div class="row">
                                 <div class="col-lg-offset-3 col-lg-7">
                                   <div class="alert alert-warning"><h4 class="text-center"> Il n'ya pas de dars pour le moment</h4> </div>
                                 </div>
                               </div>
                           </div>
                          @endforelse
                        </div>
                   </div>
               </div>
            </div>
        </div>
@endsection


