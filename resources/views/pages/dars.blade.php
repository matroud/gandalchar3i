@extends('layouts.app')
@section('extracss')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelementplayer.css">
@endsection
@section('content')
<div class="wpo-event-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title" style="margin-bottom:30px">
                            <!-- <span>Our Events</span> -->
                            <h2>{{{$dars->title}}}</h2>
                        </div>
                    </div>
                </div>
            </div>
</div>
<section class="wpo-blog-pg-section" style="margin-top:-80px">
            <div class="container">
                <div class="row">
                    <div class="col col-md-8">
                        <div class="wpo-blog-content">
                            <div class="post format-standard-image">
                                <div class="entry-media">
                                    <img src="{{{$dars->image}}}" alt  >
                                </div>
                                <ul class="entry-meta">
                                    <li><a> Ajouté par {{ $dars->user->nom}} {{ $dars->user->prenom}}</a></li>
                                    <li><a>Le {{ date('d/m/Y', strtotime($dars->created_at))}}</a></li>
                                    <li class="pull-right">nombre de visites  <i class="fi ti-eye"></i>{{views($dars)->unique()->count()}}</li>
                                </ul>
                                
                                <div class="post post-text-wrap">
                                <!-- <ul class="entry-meta">
                                    <li><a href="#"> By Robert harry</a></li>
                                    <li><a href="#"> 25 Feb 2020</a></li>
                                </ul> -->
                                <h3><a>Prérequis</a></h3>
                                <p>@if($dars->prerequi) {{$dars->prerequi}} @else Aucun preréquis @endif</p>
                                <div class="entry-bottom">
                                    <!-- <a href="#" class="read-more">S'inscrire à ce dars</a> -->
                                    <ul>
                                        <!-- <li><i class="fi flaticon-like"></i>80</li>
                                        <li><i class="fi flaticon-like"></i>60</li> -->
                                        <!-- <li><i class="fi flaticon-share"></i></li> -->
                                    </ul>
                                </div>
                            </div>
                                <div class="blog-thumb-badge">
                                    <div class="blog-thumb-text">
                                        <span>{{$dars->categorie->libelle}}</span>
                                    </div>
                                </div>
                                <div class="blog-thumb-text text-center">
                                           <span>Cheick {{$dars->cheick->kouniah}}</span>
                                        </div>
                                <div class="wpo-event-details-wrap">
                                <div class="wpo-event-details-tab">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#Schedule">Dars {{$dars->format}}</a></li>
                                        <li><a data-toggle="tab" href="#Map">Le document du dars </a></li>
                                        <li><a data-toggle="tab" href="#Contact">A propos du cheick {{$dars->cheick->kouniah}}</a></li>
                                    </ul>
                                </div>
                                <div class="wpo-event-details-content">
                                    <div class="tab-content">
                                        <div id="Schedule" class="tab-pane active">
                                        @forelse($cours as $index => $cour)
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                              <div class="media-wrapper">
                                                    <h4 class="text-center">Cours {{$index +1 }}: {{$cour->title}}</h4>
                                                    <audio id="player2" preload="none" controls class="audio_pc audio_mobile"
                                                        data-cast-title="Jazz Example"
                                                        data-cast-description="This is a description for audio only"
                                                        data-cast-poster="http://mediaelementjs.com/images/big_buck_bunny.jpg">
                                                        <source src="{{$cour->lien_dars}}" type="audio/mp3" title="{{$cour->title}}" >
                                                    </audio>
                                                    <!-- <video id="player1" width="640" height="360" preload="none" style="max-width: 100%" controls poster="images/big_buck_bunny.jpg" playsinline webkit-playsinline>
                                                        <source src="//github.com/mediaelement/mediaelement-files/blob/master/big_buck_bunny.mp4?raw=true" type="video/mp4">
                                                        <track src="dist/mediaelement.vtt" srclang="en" label="English" kind="subtitles" type="text/vtt">
                                                    </video> -->
                                              </div>
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-4 col-xs-6">
                                                        <h5>Publié le {{ date('d/m/Y', strtotime($cour->created_at))}}</h6>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6">
                                                    <!-- <h5>Durée: 1h02min</h6> -->
                                                    </div>
                                                    <div class="col-md-3 col-xs-6">
                                                        <h5><a href="{{$cour->lien_dars}}"><i class="fi ti-import"></i> Télécharger</a></h6>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                        @empty
                                        @endforelse
                                        </div>
                                        <div id="Map" class="tab-pane">
                            
                                            <div class="contact-map">
                                                <a href="{{$dars->document}}"><i class="fi ti-import"></i> Télécharger le document du dars {{$dars->title}} </a>
                                                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.9147703055!2d-74.11976314309273!3d40.69740344223377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbd!4v1547528325671" allowfullscreen></iframe> -->
                                            </div>
                                        </div>
                                        <div id="Contact" class="tab-pane">
                                            <section class="wpo-blog-single-section" style="margin-top:-60px">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col col-md-12">
                                                            <div class="wpo-wpo-blog-content clearfix">
                                                                <div class="author-box">
                                                                    <div class="author-avatar">
                                                                        <a href="#" target="_blank"><img src="/backoffice/assets/img/avatar.png" style="width:137px;height:136px" alt></a>
                                                                    </div>
                                                                    <div class="author-content">
                                                                        <a href="#" class="author-name">{{$dars->cheick->kouniah}}</a>
                                                                        <p>
                                                                           <span class="text-primary"> Nom Prénoms :</span> {{$dars->cheick->nom}} {{$dars->cheick->prenom}} <br>
                                                                           <span class="text-primary">Téléphone :</span> {{$dars->cheick->telephone}} <br>
                                                                           <span class="text-primary">Fonction :</span> {{$dars->cheick->fonction}}<br>
                                                                          <span class="text-primary"> Adresse E-mail :</span> {{$dars->cheick->email}}
                                                                        </p>
                                                                        <p>
                                                                            {{$dars->parcours}}
                                                                        </p>

                                                                        <!-- <div class="author-btn">
                                                                            <a href="#">All Post From Author</a>
                                                                        </div> -->
                                                                    </div>
                                                                </div> <!-- end author-box -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end container -->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="wpo-blog-sidebar">
                            <div class="widget profile-widget">
                                <div class="profile-img">
                                    <img src="{{$dars->chaine->logo}}" alt="">
                                    <h2>{{$dars->chaine->libelle}}</h2>
                                    <p>
                                        Téléphone :  {{$dars->chaine->telephone}} <br>
                                        Adresse E-mail :  {{$dars->chaine->email}} <br>
                                        Adresse :  {{$dars->chaine->adresse}} <br>
                                        Crée par: {{$dars->chaine->user->prenom}} {{$dars->chaine->user->nom}} <br>
                                   </p>
                                </div>
                                <div class="pro-social">
                                    <ul>
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <!-- <li><a href="#"><i class="ti-instagram"></i></a></li>
                                        <li><a href="#"><i class="ti-google"></i></a></li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="widget category-widget">
                                <h3>Categories</h3>
                                <ul>
                                    @forelse($categories as $categorie)
                                      <li><a href="{{ route('categorie_dourous',['slug_categorie' => $categorie->slug_categorie])}}">{{$categorie->libelle}}<span>({{count($categorie->dourous)}})</span></a></li>
                                    @empty
                                    @endforelse
                                    
                                </ul>
                            </div>
                            <!-- <div class="widget recent-post-widget">
                                <h3>Recent posts</h3>
                                <div class="posts">
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-1.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Salat is the best excersize for body fitness.</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-2.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Reading Quran is most important for all</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-3.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">The compleate Hajj guide for you</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
@endsection

@section('extrajs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelement-and-player.min.js"></script>
@endsection


