<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="RedstarHospital" />
	<title>Gandal char3I Inscription</title>
	<!-- google font -->
    <link href="../../../../../../fonts.googleapis.com/cssbc32.css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
		type="text/css" />
	<!-- icons -->
	<link href="/backoffice/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/backoffice/assets/plugins/iconic/css/material-design-iconic-font.min.css">
	<!-- bootstrap -->
	<link href="/backoffice/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- style -->
	<link rel="stylesheet" href="/backoffice/assets/css/pages/extra_pages.css">
	<!-- favicon -->
	<link rel="shortcut icon" href="/backoffice/assets/img/favicon.ico" />
</head>

<body>
<div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" action="{{route('login')}}" >
                    @csrf
					<span class="login100-form-logo">
						<img alt="" src="/backoffice/assets/img/logo-2.png">
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Connexion
					</span>
					<div class="form-group">
                        <label for="lastname" style="color:white">Adresse E-mail *</label>
                        <input type="email" class="form-control" id="lastname"  name="email" placeholder="Votre adresse E-mail">
                     </div>
					 <div class="form-group">
								<label for="name" style="color:white">Votre mot de passe *</label>
								<input type="password" class="form-control" name="password" id="password" placeholder="Votre mot de passe">
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Se connecter
						</button>
					</div>
					<div class="text-center p-t-30">
						<a class="txt1" href="forgot_password.html">
							Mot de passe ouublié?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- start js include path -->
	<script src="/backoffice/assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="/backoffice/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/backoffice/assets/js/pages/extra-pages/pages.js"></script>
	<!-- end js include path -->
</body>


<!-- Mirrored from radixtouch.in/templates/admin/smart/source/light/sign_up.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Jan 2021 09:18:23 GMT -->
</html>