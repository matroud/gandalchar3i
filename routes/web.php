<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PagesAdminController;
use App\Http\Controllers\ChaineController;
use App\Http\Controllers\CheickController;
use App\Http\Controllers\LangueController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\NiveauController;
use App\Http\Controllers\DourousController;
use App\Http\Controllers\DarsController;


// Route::get('/', ['as'=>'root_index', 'uses'=>'PagesController@index']);

Route::get('/', [PagesController::class, 'index'])->name('root_index');
Route::get('/dourous', [PagesController::class, 'dourous'])->name('dourous');
Route::get('/dars/{slug_dars}', [PagesController::class, 'dars'])->name('dars');
Route::get('/categorie/{slug_categorie}', [PagesController::class, 'categorie_dourous'])->name('categorie_dourous');
Route::get('/coran', [PagesController::class, 'coran'])->name('coran');
Route::get('/conferences', [PagesController::class, 'conferences'])->name('conferences');


Route::group(['prefix' => 'dashboard','middleware' => ['auth']], function () {
    Route::get('/index', [PagesAdminController::class, 'index'])->name('dashboard.index');
    Route::resource('chaine', ChaineController::class);
    Route::resource('cheick', CheickController::class);
    Route::resource('langue', LangueController::class);
    Route::resource('categorie', CategorieController::class);
    Route::resource('niveau', NiveauController::class);
    Route::resource('dourous', DourousController::class);
    Route::resource('{slug}/dars', DarsController::class);

    
});
