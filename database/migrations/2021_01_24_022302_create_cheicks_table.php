<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheicks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom',100);
            $table->string('prenom',100);
            $table->string('telephone',100)->nullable();
            $table->string('fonction',100)->nullable();
            $table->string('kouniah',100)->nullable();
            $table->string('email',100)->nullable();
            $table->text('parcours',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheicks');
    }
}
