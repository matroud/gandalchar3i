<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conferences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('slug_conference',255);
            $table->integer('cheick_id')->unsigned();
            $table->integer('langue_id')->unsigned();
            $table->string('lien',255);
            $table->string('lieu');
            $table->foreign('cheick_id')->references('id')->on('cheicks');
            $table->foreign('langue_id')->references('id')->on('langues');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conferences');
    }
}
