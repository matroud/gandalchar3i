<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('lien_dars');
            $table->string('type_dars');
            $table->string('duration')->nullable();
            $table->integer('dourous_id')->unsigned();
            $table->timestamps();
            $table->engine = 'InnoDB';

            $table->foreign('dourous_id')
            ->references('id')->on('dourouses')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dars');
    }
}
