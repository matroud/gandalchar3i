<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDourousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dourouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->string('prerequi')->nullable();
            $table->enum('statut', ['actif', 'inactif', 'bloque'])->default('inactif');
            $table->string('slug',255);
            $table->boolean('etat_cours')->nullable();
            $table->integer('nombre_dars')->nullable();
            $table->string('document')->nullable();
            $table->enum('format', ['audio', 'video'])->nullable();
            $table->integer('categorie_id')->unsigned();
            $table->integer('langue_id')->unsigned();
            $table->integer('niveau_id')->unsigned();
            $table->integer('chaine_id')->unsigned();
            $table->integer('cheick_id')->unsigned();
            $table->integer('created_by')->unsigned()->index()->nullable();
            $table->foreign('categorie_id')->references('id')->on('categories');
            $table->foreign('niveau_id')->references('id')->on('niveaux');
            $table->foreign('langue_id')->references('id')->on('langues');
            $table->foreign('chaine_id')->references('id')->on('chaines');
            $table->foreign('cheick_id')->references('id')->on('cheicks');
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dourouses');
    }
}
